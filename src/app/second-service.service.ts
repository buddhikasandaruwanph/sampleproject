import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SecondServiceService {

  private _email:string;
  private _company: string;
  private _isOk;

  constructor() { }


  getisOk() {
    return this._isOk;
  }

  setisOk(value) {
    this._isOk = value;
  }

  getemail(): string {
    return this._email;
  }

  setemail(value: string, value2: string) {
    this._email = value;
    this._company = value2;
  }

  getcompany(): string {
    return this._company;
  }

  set company(value: string) {
    this._company = value;
  }
}
