import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FistFormComponent } from './fist-form/fist-form.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {
  MatButtonModule,
  MatCardModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatStepperModule,
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {MatRadioModule} from '@angular/material/radio';
import { SecondComponent } from './second/second.component';
import { ThirdComponent } from './third/third.component';
import { FinalComponent } from './final/final.component';
import { SetapBarComponent } from './setap-bar/setap-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    FistFormComponent,
    NavBarComponent,
    SecondComponent,
    ThirdComponent,
    FinalComponent,
    SetapBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatStepperModule,
    FormsModule, ReactiveFormsModule,
    MatToolbarModule,
    MatRadioModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
