import { Component, OnInit } from '@angular/core';
import {FistServiceService} from '../fist-service.service';
import {SecondServiceService} from '../second-service.service';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit {

  userName: string;
  pasword: string;
  email: string;
  company: string;
  constructor(private fstService: FistServiceService, private secondService: SecondServiceService) { }

  ngOnInit() {
    this.userName = this.fstService.getuserName();
    this.pasword = this.fstService.getpasword();
    this.email = this.secondService.getemail();
    this.company = this.secondService.getcompany();
  }

}
