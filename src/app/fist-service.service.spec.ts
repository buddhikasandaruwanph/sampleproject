import { TestBed } from '@angular/core/testing';

import { FistServiceService } from './fist-service.service';

describe('FistServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FistServiceService = TestBed.get(FistServiceService);
    expect(service).toBeTruthy();
  });
});
