import { Component, OnInit, Inject } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgZone, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FistServiceService} from '../fist-service.service';
import {SetapBarComponent} from '../setap-bar/setap-bar.component';


@Component({
  selector: 'app-fist-form',
  templateUrl: './fist-form.component.html',
  styleUrls: ['./fist-form.component.css']
})
export class FistFormComponent implements OnInit {

  usr ='';
  pw;
  nextButtonText = 'Next';
  isOk;

  constructor(private formBuilder: FormBuilder, private ngZone: NgZone,private router:Router, private fistService: FistServiceService, private fist: SetapBarComponent) { }


  ngOnInit() {

    if(this.fistService.getuserName() && this.fistService.getpasword()){
      this.usr = this.fistService.getuserName();
      this.pw = this.fistService.getpasword();
      this.nextButtonText = "Update";
      this.fistService.setisOk(true);
      this.isOk = this.fistService.getisOk();
      console.log(this.fistService.getisOk());
    }

  }


  nextUpdateButtonClicked() {
    this.fistService.setuserName(this.usr, this.pw);
    this.fist.setVisible();
  }

}
