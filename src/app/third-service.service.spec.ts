import { TestBed } from '@angular/core/testing';

import { ThirdServiceService } from './third-service.service';

describe('ThirdServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ThirdServiceService = TestBed.get(ThirdServiceService);
    expect(service).toBeTruthy();
  });
});
