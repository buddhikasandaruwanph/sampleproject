import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FistFormComponent} from './fist-form/fist-form.component';
import {SecondComponent} from './second/second.component';
import {ThirdComponent} from './third/third.component';
import {FinalComponent} from './final/final.component';


const routes: Routes = [
  {
    path: 'fistComponent',
    component: FistFormComponent,
  },
  {
    path: 'secondComponent',
    component: SecondComponent,
  },
  {
    path: 'thirdComponent',
    component: ThirdComponent,
  },
  {
    path: 'final',
    component: FinalComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
