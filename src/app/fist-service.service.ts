import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FistServiceService {

  private _userName:string;
  private _pasword: string;
  private _isOk;

  getisOk() {
    return this._isOk;
  }

  setisOk(value) {
    this._isOk = value;
  }

  getuserName(): string {
    return this._userName;
  }

  setuserName(value: string, value2: string) {
    this._userName = value;
    this._pasword = value2;
  }

  getpasword(): string {
    return this._pasword;
  }

  setpasword(value: string) {
    this._pasword = value;
  }

  constructor() { }
}
