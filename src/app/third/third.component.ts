import { Component, OnInit } from '@angular/core';
import {SetapBarComponent} from '../setap-bar/setap-bar.component';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {

  isOk = false;

  constructor( private visible: SetapBarComponent) { }

  ngOnInit() {
  }


  AcseptClick(){
    this.isOk = true;
    this.visible.thirdCompVisible();
  }

}
