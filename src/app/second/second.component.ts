import { Component, OnInit } from '@angular/core';
import {SecondServiceService} from '../second-service.service';
import {SetapBarComponent} from '../setap-bar/setap-bar.component';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  emails;
  companys;
  nextButtonText = 'Next';
  isOk;
  constructor(private secondService :SecondServiceService, private visible: SetapBarComponent) { }

  ngOnInit() {


    if(this.secondService.getemail() && this.secondService.getcompany()){
      this.emails = this.secondService.getemail();
      this.companys = this.secondService.getcompany();
      this.nextButtonText = "Update";
      this.secondService.setisOk(true);
      this.isOk = this.secondService.getisOk();
    }
  }

  secondClick() {
    this.secondService.setemail(this.emails, this.companys);
    this.visible.secondCompVisible();
  }


}
