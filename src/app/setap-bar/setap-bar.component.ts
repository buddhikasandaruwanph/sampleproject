import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setap-bar',
  templateUrl: './setap-bar.component.html',
  styleUrls: ['./setap-bar.component.css']
})
export class SetapBarComponent implements OnInit {

  setsible = false;
  secondVisible = false;
  thirdVisible = false;

  constructor() { }

  ngOnInit() {
  }

  setVisible() {
    this.setsible = true;
  }

  secondCompVisible () {
    this.secondVisible = true;

  }

  thirdCompVisible() {
    this.thirdVisible = true;

  }

}
