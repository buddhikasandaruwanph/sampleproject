import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetapBarComponent } from './setap-bar.component';

describe('SetapBarComponent', () => {
  let component: SetapBarComponent;
  let fixture: ComponentFixture<SetapBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetapBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetapBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
